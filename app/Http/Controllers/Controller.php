<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Aosmak\Laravel\Layer\Sdk\Services\LayerServiceInterface;


class Controller extends BaseController
{
    public function getToken(Request $request)
    {
        $userId = $request->input('user_id');
        $nonce = $request->input('nonce');

        $tokenProvider = new \Layer\LayerIdentityTokenProvider();
        $identityToken = $tokenProvider->generateIdentityToken($userId, $nonce);


        return \GuzzleHttp\json_encode(['token' => $identityToken]);
    }

    /**
     * Test method
     *
     * @param \Aosmak\Laravel\Layer\Sdk\Services\LayerService $layer
     *
     * @return void
     */
    public function test(LayerServiceInterface $layer)
    {
        $result = [
            $layer->getUserService()->get('user_1'),
            $layer->getUserService()->get('user_2'),
        ];

        return \GuzzleHttp\json_encode($result);
    }
}
