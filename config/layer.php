<?php

return [
    'LAYER_SDK_APP_ID'           => env('LAYER_SDK_APP_ID', 'dae88100-d7e1-11e6-b424-d04e02005753'),
    'LAYER_SDK_AUTH'             => env('LAYER_SDK_AUTH', 'O0smrCJGMpZJMO7NzPLr8EVKAzbStjXWy7nyB4yb0xZ95lO0'),
    'LAYER_SDK_BASE_URL'         => env('LAYER_SDK_BASE_URL', 'https://api.layer.com/apps/'),
    'LAYER_SDK_SHOW_HTTP_ERRORS' => env('LAYER_SDK_SHOW_HTTP_ERRORS', true),
];